# Concurrent Forward List

Fine-grained locking singly-linked list implemented in C++17.

## Building and Testing

This project uses CMake. To test the build and run tests, start by creating a
`build` directory and initialize CMake in that directory:

```
mkdir build
cd build
cmake ..
```

Afterwards, still inside `build` directory, run

```
cmake --build .
```

to build the test target, then run

```
ctest
```

to execute the test.

## Importing Into Your Project

There are many ways to do this.

1.  Copying the source code. This is in fact quite easy to do because this
    library is a header-only library. Just copy everything inside [src](src)
    and place it wherever you like.

2.  Copying the whole repository. This will work fine if you use CMake and you
    add the root of this repository with `add_subdirectory`.

3.  As a variation on 2, you can also add this repository as a git submodule to
    your git repository.

