#pragma once

#include <atomic>
#include <cassert>
#include <cstddef>
#include <mutex>
#include <shared_mutex>
#include <type_traits>
#include <utility>
#include <variant>

namespace concurrent_forward_list {

/** @brief
 *  Singly-linked list data structure with fine-grained locking.
 *
 *  This is a single-linked list data structure in which each entry has its own
 *  mutex. Each entry contains a value, which can be accessed by the thread
 *  that locks the entry. Inserting a entry requires one entry to be locked.
 *  Removing a entry requires two adjacent entrys to be locked.
 *  Any operations on entrys can be done concurrently as long as appropriate
 *  locks are held.
 *
 *  @tparam ValueT Type of values in list's entries.
 *  @tparam MutexT Type of mutexes in list's entires.
 *    This is typically `std::mutex` or `std::shared_mutex`.
 *    If this is `std::shared_mutex`, then each entry may be read concurrently
 *    by different threads.
 *  @tparam UniqueLockT Type of locks that will be used on mutexes for
 *    operations that modify the list.
 *    The default value is `std::unique_lock<MutexT>`.
 *  @tparam SharedLockT Type of locks that will be used on mutexes for reading.
 *    If `MutexT` is `std::shared_mutex`, the default value of `SharedLockT`
 *    will be `std::shared_lock<MutexT>`.
 *    Otherwise, the default value of `SharedLockT` will be
 *    `std::unique_lock<MutexT>`.
 */
template <class ValueT, class MutexT = std::shared_mutex,
          class UniqueLockT = std::unique_lock<MutexT>,
          class SharedLockT = std::conditional_t<
              std::is_same_v<MutexT, std::shared_mutex>,
              std::shared_lock<MutexT>, std::unique_lock<MutexT>>>
class ConcurrentForwardList {
public:
  /// Type of values stored in list's entries.
  using Value = ValueT;
  /// Type of mutex to use for locking list's entries.
  using Mutex = MutexT;
  /** @brief
   *  Type of lock to use for reading and writing.
   */
  using UniqueLock = UniqueLockT;
  /** @brief
   *  Type of lock to use for reading.
   */
  using SharedLock = SharedLockT;
  /// This type.
  using This = ConcurrentForwardList<Value, Mutex, UniqueLock, SharedLock>;

  /// `std::size_t`.
  using size_type = std::size_t;

protected:
  struct Node {
    // This will store `size` for the sentinel node.
    // For non-sentinel nodes, it will store `value`.
    mutable std::variant<size_type, Value> size_or_value;

    // This `mutex` protects `next` and its members.
    // For the sentinel node, this mutex also protects `size()`.
    mutable Mutex mutex;

    // `next` is not initialized by the constructor. It must be explicitly set
    // after creation.
    Node* next;

    // Passes all arguments to size_or_value.
    template <class... ArgsT>
    constexpr Node(ArgsT&&... args)
        : size_or_value(std::forward<ArgsT>(args)...) {}

    // Returns the reference to the `size` part of `size_or_value`.
    constexpr size_type& size() const { return std::get<0>(size_or_value); }

    // Returns the reference to the `value` part of `size_or_value`.
    constexpr Value& value() const { return std::get<1>(size_or_value); }
  };

  // `sentinel_` will be `Node` with `size()` but not `value()`.
  std::unique_ptr<Node> sentinel_{new Node(std::in_place_index<0>, 0)};

  // This mutex protects `sentinel_` pointer.
  mutable Mutex mutex_;

public:
  /** @brief
   *  Creates an empty list.
   */
  constexpr ConcurrentForwardList() { sentinel_->next = nullptr; }

  /** @brief
   *  Returns the size of this list.
   *
   *  @returns Size of this list.
   */
  constexpr size_type size() const {
    SharedLock main_lock{mutex_};
    Node* sentinel{sentinel_.get()};
    SharedLock sentinel_lock{sentinel->mutex};
    main_lock.unlock();
    return sentinel->size();
  }

  /** @brief
   *  Checks if this list is empty.
   *
   *  @returns `true` if and only if this list is empty.
   */
  constexpr bool empty() const { return size() == 0; }

  /** @brief
   *  Clears this list. If @p async is `false`, this function will return a
   *  lock that owns the mutex for the whole list, preventing any modification
   *  when this function returns. If @p async is `true`, the list may not be
   *  empty when this function returns, and the returned lock will not own a
   *  mutex.
   *
   *  `clear()` will lock every entry before deleting. It may deadlock if there
   *  is an active lock somewhere.
   *
   *  @param async Whether the clear operation should be asynchronous or not.
   */
  constexpr UniqueLock clear(bool async = true) {
    UniqueLock main_lock{mutex_};
    Node* sentinel{sentinel_.get()};
    UniqueLock sentinel_lock{sentinel->mutex};
    Node* cur_node{sentinel->next};
    sentinel_->next = nullptr;
    sentinel_->size() = 0;
    sentinel_lock.unlock();
    if (async) {
      main_lock.unlock();
    }

    // At this point, the list seems empty to other threads.
    // If `async` is `true`, other threads can modify the list right away.
    // If `async` is `false`, other threads will not be able to access
    // `sentinel_` until `main_lock` unlocks.

    // Cleaning up is done after `sentinel_->next` has been disconnected.
    while (cur_node) {
      UniqueLock cur_lock{cur_node->mutex};
      Node* next_node{cur_node->next};
      cur_lock.unlock();
      delete cur_node;
      cur_node = next_node;
    }

    // If `async` is `false`, the returned lock will keep the list empty and
    // prevent `sentinel_` from changing.
    return main_lock;
  }

  ~ConcurrentForwardList() { clear(); }

  /** @brief
   *  Copies values from another list.
   *
   *  This copy operation has two stages: clearing this list, and copying
   *  entries from `s`. During the first stage, all other threads are blocked
   *  from modifying this last. During the second stage, other threads may
   *  modify this list concurrently.
   *
   *  @param s Source list.
   *  @returns Reference to this list.
   */
  constexpr This& operator=(This const& s) {
    // Clear the list synchronously, then retrieve sentinel_.
    UniqueLock main_lock{clear(false)};
    Node* sentinel{sentinel_.get()};
    Node* prev_node{sentinel};
    UniqueLock prev_lock{prev_node->mutex};
    main_lock.unlock();
    // `sentinel_` is allowed to change from this point onwards.
    // The copy operation will be done on `sentinel`, which has the original
    // value of `sentinel_`.

    size_type count{0};  // This will keep the number of nodes copied.

    // The source list only needs a shared lock.
    SharedLock s_main_lock{s.mutex_};
    SharedLock s_prev_lock{s.sentinel_->mutex};
    Node* s_cur_node{s.sentinel_->next};
    s_main_lock.unlock();
    // At this point, `s.sentinel_` is allowed to change too because we have
    // already retrieved the pointer to the first node and stored it in
    // `s_cur_node`.

    while (s_cur_node) {
      // Loop invariants at this point:
      // - prev_node is the last node of this list.
      // - prev_lock is held, so prev_node->next can be used.
      // - s_cur_node is the node we want to copy and add as prev_node->next.
      // - s_prev_lock is held, so s_cur_node can be used.

      // Create a node whose value is copied from `s_cur_node` and put it in
      // prev_node->next.
      Node* cur_node{new Node(std::in_place_index<1>, s_cur_node->value())};
      prev_node->next = cur_node;

      // Move to the next node in s.
      SharedLock s_cur_lock{s_cur_node->mutex};
      s_cur_node = s_cur_node->next;
      s_prev_lock.unlock();
      s_prev_lock = std::move(s_cur_lock);

      // Move to the next node in this.
      prev_node = cur_node;
      UniqueLock cur_lock{cur_node->mutex};
      prev_lock.unlock();
      prev_lock = std::move(cur_lock);

      // Keep count of the number of new nodes.
      ++count;
    }
    // prev_lock and s_prev_lock are still held.
    prev_node->next = nullptr;
    prev_lock.unlock();
    s_prev_lock.unlock();

    {
      UniqueLock sentinel_lock{sentinel->mutex};
      sentinel->size() += count;
    }
    return *this;
  }

  constexpr ConcurrentForwardList(This const& other) {
    sentinel_->next = nullptr;
    operator=(other);
  }

  /** @brief
   *  Moves entries from another list into this list.
   *
   *  @param s Source list.
   *  @returns Reference to this list.
   */
  constexpr This& operator=(This&& s) {
    UniqueLock main_lock{clear(false)};
    UniqueLock s_main_lock{s.mutex_};
    sentinel_.swap(s.sentinel_);
    return *this;
  }

  constexpr ConcurrentForwardList(This&& other) {
    sentinel_->next = nullptr;
    operator=(std::move(other));
  }

  /** @brief
   *  Swaps entries with another list.
   *
   *  @param s Another list.
   */
  constexpr void swap(This& s) {
    UniqueLock main_lock{mutex_};
    UniqueLock s_main_lock{s.mutex_};
    sentinel_.swap(s.sentinel_);
  }

  /** @brief
   *  Iterator class for `ConcurrentForwardList`.
   *
   *  This class contains a reference to an entry in the list as well as a lock
   *  for the mutex associated to the entry. (It implements RAII via a data
   *  member of type `LockT`.)
   *
   *  A `LockedEntry` is usually created with an active lock, i.e., with a lock
   *  that owns the mutex. When a `LockedEntry` goes out of scope, the mutex is
   *  unlocked as well.
   *
   *  The user can choose to unlock early by calling `unlock()` themselves.
   *  Most operations of `LockedEntry` will have undefined behaviors after the
   *  mutex is unlocked.
   *
   *  @tparam LockT Either `UniqueLock` or `SharedLock`.
   *    `SharedLock` is meant for read-only operations.
   *    `UniqueLock` is meant for read and write operations.
   */
  template <class LockT>
  class LockedEntry {
  public:
    /** @brief
     *  Type of lock, which should be either `UniqueLock` or `SharedLock`.
     */
    using Lock = LockT;

    /** @brief
     *  This class.
     */
    using This = LockedEntry<Lock>;

    /** @brief
     *  Type of the owning list.
     */
    using Owner = ConcurrentForwardList<Value, Mutex, UniqueLock, SharedLock>;

    /** @brief
     *  Whether `LockT` is `UniqueLock` or not.
     *  In other words, `is_unique` is `true` if and only if this `LockedEntry`
     *  can be used to modify the list.
     */
    static constexpr bool is_unique = std::is_same_v<Lock, UniqueLock>;

    /** @brief
     *  Checks whether this `LockedEntry` owns a mutex or not.
     *
     *  There are two situations where this function may return `false`:
     *  1. This `LockedEntry` was default-constructed.
     *  2. `unlock()` has been called.
     *
     *  @returns `true` iff this `LockedEntry` currently owns the mutex.
     */
    constexpr bool ownsLock() const { return prev_lock_.owns_lock(); }

    /** @brief
     *  Unlocks the mutex.
     *
     *  The lock must currently own the mutex or the behavior will be
     *  undefined.
     */
    constexpr void unlock() { prev_lock_.unlock(); }

    ///@{
    /** @brief
     *  Checks whether this `LockedEntry` points to a valid value or not.
     *
     *  There are two situations where `hasValue()` may return `false`:
     *  1. This `LockedEntry` was default-constructed.
     *  2. This `LockedEntry` points beyond the last element of the list.
     *
     *  `LockedEntry` objects that don't have a value will all compare equal
     *  by `operator==`. That means a default-constructed `LockedEntry` can be
     *  used as the `end()` iterator in forward iteration.
     *
     *  @returns `true` if this `LockedEntry` has a value, in which case
     *    `value()` and `operator *` can be called; or `false` otherwise.
     */
    constexpr bool hasValue() const noexcept { return cur_node_; }
    constexpr operator bool() const noexcept { return hasValue(); }
    ///@}

    ///@{
    /** @brief
     *  Returns the reference to the pointed value.
     *
     *  The behavior is undefined if `hasValue()` is `false`.
     *
     *  @returns Reference to the pointed value.
     */
    constexpr Value& value() const noexcept {
      assert(hasValue());
      return cur_node_->value();
    }
    constexpr Value& operator*() const noexcept { return value(); }
    ///@}

    /** @brief
     *  Returns the pointer to the pointed value.
     *
     *  The behavior is undefined if `hasValue()` is `false`.
     *
     *  @returns Pointer to the pointed value.
     */
    constexpr Value* operator->() const noexcept { return &value(); }

    /** @brief
     *  Locks the next entry and returns a `LockedEntry` holding that lock.
     *
     *  The behavior is undefined if `hasValue()` or `ownsLock()` is `false`.
     *
     *  @returns `LockedEntry` for the next entry.
     */
    constexpr This next() const {
      assert(ownsLock() && hasValue());
      return This{sentinel_, cur_node_, Lock{cur_node_->mutex}};
    }

    /** @brief
     *  Locks the next @p n entries and returns a `LockedEntry` holding the
     *  `n`-th lock.
     *
     *  The behavior is undefined if `ownsLock()` is `false`.
     *
     *  @param n
     *  @returns `LockedEntry` for the next `n`-th entry.
     */
    template <class NumT, std::enable_if_t<std::is_arithmetic_v<NumT>, int> = 0>
    constexpr This operator+(NumT n) const {
      if (n == 0) {
        return {*this};
      }
      This result{next()};
      for (; n > 1; n -= 1) {
        result = result.next();
      }
      return result;
    }

    /** @brief
     *  Moves this `LockedEntry` to lock the next entry instead.
     *
     *  The behavior is undefined if `hasValue()` or `ownsLock()` is `false`.
     *
     *  This function will do the following operations sequentially:
     *  1. Lock the next entry.
     *  2. Release the current entry.
     *  3. Move pointers to the next entry.
     */
    constexpr void advance() {
      assert(ownsLock() && hasValue());
      Lock cur_lock{cur_node_->mutex};
      prev_node_ = cur_node_;
      cur_node_ = cur_node_->next;
      prev_lock_.unlock();
      prev_lock_ = std::move(cur_lock);
    }

    /** @brief
     *  Calls `advance()`, then returns `*this`.
     *
     *  @returns `*this`.
     */
    constexpr This& operator++() {
      advance();
      return *this;
    }

    /** @brief
     *  Calls `advance()` @p steps times, then returns `*this`.
     *
     *  @param steps
     *  @return `*this`.
     */
    constexpr This& operator+=(size_type steps) {
      for (; steps; --steps) {
        advance();
      }
      return *this;
    }

    /** @brief
     *  Post-increments version of `operator++`.
     *
     *  @returns `LockedEntry` before moving `*this` to lock the next entry.
     */
    constexpr This operator++(int) {
      This other{next()};
      swap(other);
      return other;
    }

    /** @brief
     *  Checks if this `LockedEntry` is equal to `other`.
     *
     *  This check does not take into account the lock status, i.e., an
     *  unlocked entry may compare equal to a locked entry.
     *
     *  All `LockedEntry` objects that do not point to a valid value are
     *  considered to be equal.
     *
     *  If `is_unique` is `true`, `operator==` should only return `true` when
     *  both operands (`*this` and `other`) do not have a value.
     *
     *  @param other
     *  @returns `true` if `*this` and `other` point to the same entry, or if
     *    both of them do not point to a valid entry.
     */
    constexpr bool operator==(This const& other) const {
      return cur_node_ == other.cur_node_;
    }

    /** @brief
     *  Checks if this `LockedEntry` is not equal to `other`.
     *
     *  @param other
     *  @returns Negation of `*this == other`.
     */
    constexpr bool operator!=(This const& other) const {
      return !operator==(other);
    }

    /** @brief
     *  Inserts an entry whose value is constructed from `args...` into the
     *  list right before the pointed entry.
     *  This `LockedEntry` will point to the new entry afterwards.
     *
     *  The behavior of `emplace()` is undefined if `ownsLock()` returns
     *  `false`.
     *
     *  To obtain a `LockedEntry` pointed to the old entry, simply call
     *  `next()` afterwards.
     *
     *  Calling `emplace()` on a past-the-end `LockedEntry` will insert a new
     *  entry at the end of the list.
     *  Note that even though a past-the-end `LockedEntry` compares equal to a
     *  default-constructed `LockedEntry`, a default-constructed `LockedEntry`
     *  cannot be used to insert a new entry because it does not own a lock.
     *
     *  @param args Parameters that will be passed to the constructor of
     *    `Value`.
     */
    template <class... ArgsT, class LT = Lock>
    constexpr void emplace(ArgsT&&... args) {
      static_assert(is_unique, "emplace() is only available in UniqueEntry");
      assert(ownsLock());
      Node* new_node{
          new Node(std::in_place_index<1>, std::forward<ArgsT>(args)...)};
      new_node->next = prev_node_->next;
      cur_node_ = new_node;
      prev_node_->next = cur_node_;

      if (prev_node_ == sentinel_) {
        ++sentinel_->size();
        return;
      }
      UniqueLock sentinel_lock{sentinel_->mutex};
      ++sentinel_->size();
    }

    /** @brief
     *  Removes the current entry from the list and moves this `LockedEntry` to
     *  lock the next entry.
     *
     *  The behavior of `erase()` is undefined if `ownsLock()` or `hasValue()`
     *  returns `false`.
     */
    template <class LT = Lock>
    constexpr void erase() {
      static_assert(is_unique, "erase() is only available in UniqueEntry");
      assert(ownsLock() && hasValue());

      Lock cur_lock{cur_node_->mutex};
      Node* next_node{cur_node_->next};
      cur_lock.unlock();

      prev_node_->next = next_node;
      cur_node_ = next_node;

      if (prev_node_ == sentinel_) {
        --sentinel_->size();
        return;
      }
      UniqueLock sentinel_lock{sentinel_->mutex};
      --sentinel_->size();
    }

    /** @brief
     *  Default constructor.
     *
     *  A default-constructed `LockedEntry` will not point to a valid value,
     *  nor will it own a mutex. It will compare equal to a past-the-end
     *  `LockedEntry`, so it can be used as the *end iterator* in a loop.
     *  However, it cannot be used to locate the end of the list for insertion.
     *  (`emplace()` does not work if the `LockedEntry` does not own a mutex
     *  anyway.)
     */
    constexpr LockedEntry() noexcept = default;

    ///@{
    /** @brief
     *  A `LockedEntry` can only be copied if `is_unique` is `false`, i.e., if
     *  it is not a `UniqueEntry`.
     *
     *  @param other
     */
    constexpr LockedEntry(This const& other)
        : sentinel_{other.sentinel_},
          prev_node_{other.prev_node_},
          prev_lock_{(other.ownsLock() && prev_node_) ? Lock{prev_node_->mutex}
                                                      : Lock{}},
          cur_node_{other.cur_node_} {
      static_assert(!is_unique, "UniqueEntry cannot be copied");
    }
    constexpr This& operator=(This const& other) {
      static_assert(!is_unique, "UniqueEntry cannot be copied");
      sentinel_ = other.sentinel_;
      prev_node_ = other.prev_node_;
      if (other.ownsLock()) {
        prev_lock_ = Lock{prev_node_->mutex};
      } else if (prev_lock_.owns_lock()) {
        prev_lock_.unlock();
      }
      cur_node_ = other.cur_node_;
      return *this;
    }
    ///@}

    ///@{
    /** @brief
     *  Move everything from @p other.
     *  @p other will not own a mutex afterwards.
     *
     *  @param other
     */
    constexpr LockedEntry(This&& other) = default;
    constexpr This& operator=(This&& other) = default;
    ///@}

    /** @brief
     *  Swaps all data members with @p other.
     *
     *  @param other
     */
    constexpr void swap(This& other) noexcept {
      std::swap(sentinel_, other.sentinel_);
      std::swap(prev_node_, other.prev_node_);
      prev_lock_.swap(other.prev_lock_);
      std::swap(cur_node_, other.cur_node_);
    }

  protected:
    Node* sentinel_{nullptr};
    Node* prev_node_{nullptr};
    Lock prev_lock_{};

    // This is a cached value. It is always equal to `prev_node_->next` except
    // when `prev_node_` is null.
    Node* cur_node_{nullptr};

    // Internal constructor.
    constexpr LockedEntry(Node* sentinel, Node* prev_node, Lock&& prev_lock)
        : sentinel_{sentinel},
          prev_node_{prev_node},
          prev_lock_{std::move(prev_lock)},
          cur_node_{prev_node->next} {}

    friend Owner;
    friend class ConcurrentForwardListProbe;
  };

  /** @brief
   *  `LockedEntry<UniqueLock>`. This is used for read and write operations.
   */
  using UniqueEntry = LockedEntry<UniqueLock>;
  /** @brief
   *  `LockedEntry<SharedLock>`. This is used for read operations.
   */
  using SharedEntry = LockedEntry<SharedLock>;

  ///@{
  /** @brief
   *  Locks the first entry for reading and writing.
   *
   *  @returns `UniqueEntry` for the first entry.
   *    This will point *past the end* if the list is empty.
   */
  constexpr UniqueEntry beginUnique() { return staticBegin<UniqueLock>(this); }
  constexpr UniqueEntry begin() { return beginUnique(); }
  ///@}

  ///@{
  /** @brief
   *  Locks the first entry for reading.
   *
   *  @returns `SharedEntry` for the first entry.
   *    This will point *past the end* if the list is empty.
   */
  constexpr SharedEntry beginShared() const {
    return staticBegin<SharedLock>(this);
  }
  constexpr SharedEntry begin() const { return beginShared(); }
  constexpr SharedEntry cbegin() const { return begin(); }
  ///@}

  ///@{
  /** @brief
   *  Returns a *null* `UniqueEntry`, i.e., a `UniqueEntry` that does not
   *  lock any entry.
   *
   *  The return value cannot be used to add an entry to the end of the list
   *  because it does not own any mutex.
   *  The sole purpose of this is to allow range-based for loop to work.
   *
   *  @return `UniqueEntry` that does not lock anything.
   */
  constexpr UniqueEntry endUnique() { return staticEnd<UniqueLock>(this); }
  constexpr UniqueEntry end() { return endUnique(); }
  ///@}

  ///@{
  /** @brief
   *  Returns a *null* `SharedEntry`, i.e., a `SharedEntry` that does not
   *  lock any entry.
   *
   *  The sole purpose of this is to allow range-based for loop to work.
   *
   *  @return `SharedEntry` that does not lock anything.
   */
  constexpr SharedEntry endShared() const {
    return staticEnd<SharedLock>(this);
  }
  constexpr SharedEntry end() const { return endShared(); }
  constexpr SharedEntry cend() const { return end(); }
  ///@}

  ///@{
  /** @brief
   *  Locks the first entry whose value satisfies the given predicate @p p
   *  for reading and writing.
   *
   *  @param p Callable object such that `p(value)` returns `true` when `value`
   *    satisfies the desired condition.
   *  @returns `UniqueEntry` that locks the first entry whose value satisfies
   *    @p p . If no values satisfy @p p , the past-the-end `UniqueEntry` with
   *    a lock will be returned. Calling `emplace()` on this past-the-end
   *    `UniqueEntry` will add an entry to the end of the list.
   */
  template <class PredicateT>
  constexpr UniqueEntry findUnique(PredicateT&& p) {
    return staticFind<UniqueLock>(this, std::forward<PredicateT>(p));
  }
  template <class PredicateT>
  constexpr UniqueEntry find(PredicateT&& p) {
    return findUnique(std::forward<PredicateT>(p));
  }
  ///@}

  ///@{
  /** @brief
   *  Locks the first entry whose value satisfies the given predicate @p p
   *  for reading.
   *
   *  @param p Callable object such that `p(value)` returns `true` when `value`
   *    satisfies the desired condition.
   *  @returns `SharedEntry` that locks the first entry whose value satisfies
   *    @p p . If no values satisfy @p p , the past-the-end `SharedEntry` with
   *    a lock will be returned.
   */
  template <class PredicateT>
  constexpr SharedEntry findShared(PredicateT&& p) const {
    return staticFind<SharedLock>(this, std::forward<PredicateT>(p));
  }
  template <class PredicateT>
  constexpr SharedEntry find(PredicateT&& p) const {
    return findShared(std::forward<PredicateT>(p));
  }
  ///@}

  /** @brief
   *  Retrieves a `UniqueEntry` locking the past-the-end location.
   *  This can be used for adding an entry to the end of the list.
   *
   *  This function needs to lock all entries before locking the past-the-end
   *  location, so it will block if any entries are still locked somewhere
   *  else.
   *
   *  Note that there is no `const` variant of this function because there is
   *  no use of a `SharedEntry` locking the past-the-end location.
   *
   *  @returns `UniqueEntry` that locks the past-the-end location.
   */
  constexpr UniqueEntry findEnd() {
    UniqueEntry entry{begin()};
    for (; entry; entry.advance()) {
    }
    return entry;
  }

  /** @brief
   *  Inserts a new entry with a value constructed from `args...` to the front
   *  of the list.
   *
   *  @param args Parameters that will be passed to the constructor of `Value`.
   *  @return `UniqueEntry` locking the newly constructed entry.
   */
  template <class... ArgsT>
  constexpr UniqueEntry emplaceFront(ArgsT&&... args) {
    UniqueEntry entry{begin()};
    entry.emplace(std::forward<ArgsT>(args)...);
    return entry;
  }

  /** @brief
   *  Inserts a new entry with a value constructed from `args...` to the front
   *  of the first entry whose value satisfies a given predicate.
   *
   *  @param p Callable object such that `p(value)` returns `true` when `value`
   *    satisfies a desired condition.
   *  @param args Parameters that will be passed to the constructor of `Value`.
   *  @return `UniqueEntry` locking the newly constructed entry.
   */
  template <class PredicateT, class... ArgsT>
  constexpr UniqueEntry emplaceBefore(PredicateT&& p, ArgsT&&... args) {
    UniqueEntry entry{find(std::forward<PredicateT>(p))};
    entry.emplace(std::forward<ArgsT>(args)...);
    return entry;
  }

  /** @brief
   *  Inserts a new entry with a value constructed from `args...` to the back
   *  of the list.
   *
   *  @param args Parameters that will be passed to the constructor of `Value`.
   *  @return `UniqueEntry` locking the newly constructed entry.
   */
  template <class... ArgsT>
  constexpr UniqueEntry emplaceBack(ArgsT&&... args) {
    UniqueEntry entry{findEnd()};
    entry.emplace(std::forward<ArgsT>(args)...);
    return entry;
  }

  /** @brief
   *  Removes the first entry whose value satisfies the given predicate @p p
   *  and returns a `UniqueLock` for the entry following the removed one.
   *  If no entries satisfy @p p , the returned `UniqueLock` will lock the
   *  past-the-end location.
   *
   *  To distinguish between the case where the last entry is removed and the
   *  case where no entries satisfy @p p , the return value will also contain a
   *  boolean value indicating whether an entry has been removed or not.
   *  In both cases, the returned `UniqueLock` will lock the past-the-end
   *  location, so it can be used to insert an entry to the end of the list.
   *
   *  @param p Callable object such that `p(value)` returns `true` when `value`
   *    satisfies the desired condition.
   *  @returns Pair consisting of a `UniqueEntry` locking the entry after the
   *    removed entry or the past-the-end entry, and a boolean value telling
   *    whether an entry satisfying @p p was found and removed.
   */
  template <class PredicateT>
  constexpr std::pair<UniqueEntry, bool> erase(PredicateT&& p) {
    UniqueEntry entry{find(std::forward<PredicateT>(p))};
    if (entry) {
      entry.erase();
      return {std::move(entry), true};
    }
    return {std::move(entry), false};
  }

  /** @brief
   *  Removes the first entry from this list and returns a `UniqueLock` locking
   *  the next entry (which will be the new first entry), or returns an empty
   *  `UniqueEntry` if the list is empty.
   *
   *  To distinguish between the case where the first entry is removed and the
   *  case where the list is empty, the return value will also contain a
   *  boolean value indicating whether the first entry has been removed or not.
   *  In both cases, the returned `UniqueLock` will lock the new first entry
   *  (which may be the past-the-end location), so it can be used to insert an
   *  entry to the front of the list.
   *
   *  Note: This function behaves exactly like
   *    `erase([](Value const&) { return true; })`.
   *
   *  @returns Pair consisting of a `UniqueEntry` locking the new first entry
   *    (regardless of whether it existed and was removed or not), and a
   *    boolean value telling whether an entry was removed or not.
   */
  constexpr std::pair<UniqueEntry, bool> popFront() {
    UniqueEntry entry{begin()};
    if (entry) {
      entry.erase();
      return {std::move(entry), true};
    }
    return {std::move(entry), false};
  }

protected:
  // The following functions are made static to cover the case where `ThisT` is
  // `This*` and `This const*` at the same time.

  // Static version of `begin()`.
  template <class LockT, class ThisT>
  static constexpr LockedEntry<LockT> staticBegin(ThisT t) {
    LockT main_lock{t->mutex_};
    return LockedEntry<LockT>{t->sentinel_.get(), t->sentinel_.get(),
                              LockT{t->sentinel_->mutex}};
  }

  // Static version of `end()`.
  template <class LockT, class ThisT>
  static constexpr LockedEntry<LockT> staticEnd(ThisT t) {
    return LockedEntry<LockT>{};
  }

  // Static version of `find()`.
  template <class LockT, class ThisT, class PredicateT>
  static constexpr LockedEntry<LockT> staticFind(ThisT t, PredicateT&& p) {
    LockedEntry<LockT> entry{staticBegin<LockT>(t)};
    for (; entry && !p(*entry); entry.advance()) {
    }
    return entry;
  }

  friend class ConcurrentForwardListProbe;
};

template <class Value>
using BasicConcurrentForwardList = ConcurrentForwardList<Value, std::mutex>;

template <class Value>
using ReadHeavyConcurrentForwardList =
    ConcurrentForwardList<Value, std::shared_mutex>;

}  // namespace concurrent_forward_list
