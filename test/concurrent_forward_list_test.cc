#include <gtest/gtest.h>
#include <concurrent_forward_list/concurrent_forward_list.h>

#include <algorithm>
#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <utility>

using namespace std;

using concurrent_forward_list::ConcurrentForwardList;

template <class B1, class E1, class B2, class E2>
size_t compareList(B1&& b_1, E1&& e_1, B2&& b_2, E2&& e_2) {
  for (; (b_1 != e_1) && (b_2 != e_2); ++b_1, ++b_2) {
    if (*b_1 < *b_2) {
      return -1;
    }
    if (*b_1 > *b_2) {
      return 1;
    }
  }
  if (b_1 != e_1) {
    return 1;
  }
  if (b_2 != e_2) {
    return -1;
  }
  return 0;
}

TEST(ConcurrentForwardList, SingleThreaded) {
  using List = ConcurrentForwardList<size_t, shared_mutex>;

  List l;
  // -- `l` should be empty.
  EXPECT_TRUE(l.empty());

  // Test emplaceFront().
  // > Add [0, 1, 2, 3, 4] to `l`.
  l.emplaceFront(4);
  l.emplaceFront(3);
  l.emplaceFront(2);
  l.emplaceFront(1);
  l.emplaceFront(0);

  // -- `l` should be [0, 1, 2, 3, 4].
  EXPECT_EQ(l.size(), 5);

  {
    // Test iteration.
    size_t i = 0;
    for (auto const& x : as_const(l)) {
      EXPECT_EQ(i, x);
      ++i;
    }
    // Test non-const iteration.
    // > Add 1 to all elements of `l`.
    for (auto& x : l) {
      ++x;
    }
    // -- `l` should be [1, 2, 3, 4, 5].
    i = 1;
    for (auto const& x : as_const(l)) {
      EXPECT_EQ(i, x);
      ++i;
    }

    // Test emplaceBack().
    // > Add [6, 7, 8, 9, 10] to `l`.
    for (i = 6; i <= 10; ++i) {
      l.emplaceBack(i);
    }
    // -- `l` should be [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].

    i = 1;
    for (auto const& x : as_const(l)) {
      EXPECT_EQ(i, x);
      ++i;
    }
  }

  {  // Test copying, moving, and swapping.
    // > Copy `l` to `l_1`.
    List l_1{l};
    // -- `l_1` should be equal to `l`.
    EXPECT_EQ(compareList(l.begin(), l.end(), l_1.begin(), l_1.end()), 0);

    // > Move `l` to `l_2`.
    List l_2{std::move(l)};
    // -- `l_2` should be equal to `l_1` while `l` should be empty.
    EXPECT_TRUE(l.empty());
    EXPECT_EQ(compareList(l_1.cbegin(), l_1.cend(), l_2.cbegin(), l_2.cend()),
              0);

    // > Swap `l` with `l_2`.
    l.swap(l_2);
    // -- `l` should be equal to `l_1` while `l_2` should be empty.
    EXPECT_TRUE(l_2.empty());
    EXPECT_EQ(compareList(l.begin(), l.end(), l_1.begin(), l_1.end()), 0);
  }

  // -- `l` should be [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].

  // Test `findShared()`.
  {
    vector<List::SharedEntry> shared_entries;
    for (size_t i{1}; i <= 10; ++i) {
      List::SharedEntry shared_entry{
          as_const(l).find([i](size_t x) {
        return x == i; })};
      shared_entries.emplace_back(shared_entry);
    }
    // Verify that elements of `shared_entries` are valid.
    for (size_t i{0}; i < 10; ++i) {
      EXPECT_TRUE(shared_entries[i].hasValue());
      EXPECT_TRUE(shared_entries[i].ownsLock());
      EXPECT_EQ(*shared_entries[i], i + 1);
    }
  }

  // Test `erase()`.
  // > Remove [3, 6, 9] from `l`.
  for (size_t i{0}; i < 3; ++i) {
    auto erase_result{l.erase([](size_t x) { return x % 3 == 0; })};
    EXPECT_TRUE(erase_result.second);
    auto& locked_entry{erase_result.first};
    EXPECT_TRUE(locked_entry.hasValue());
    EXPECT_TRUE(locked_entry.ownsLock());
    // Verify that `locked_entry` is `4`, `7`, or `10`.
    EXPECT_EQ((locked_entry.value() - 1) % 3, 0);
  }
  // -- `l` should be [1, 2, 4, 5, 7, 8, 10].
  EXPECT_EQ(l.size(), 7);

  // > Remove `1`.
  {
    auto erase_result{l.erase([](size_t x) { return x == 1; })};
    EXPECT_TRUE(erase_result.second);
  }
  // -- `l` should be [2, 4, 5, 7, 8, 10].
  // > Remove `1` again. It should fail this time.
  {
    auto erase_result{l.erase([](size_t x) { return x == 1; })};
    EXPECT_FALSE(erase_result.second);
    auto& locked_entry{erase_result.first};
    EXPECT_FALSE(locked_entry.hasValue());
    EXPECT_TRUE(locked_entry.ownsLock());
    // > Add 11 to the end of `l`.
    // -- `l` should be [2, 4, 5, 7, 8, 10, 11].
    locked_entry.emplace(11);
    EXPECT_TRUE(locked_entry.hasValue());
    EXPECT_EQ(*locked_entry, 11);
  }

  {  // Test `emplaceBefore()`.
    auto emplace_result{l.emplaceBefore([](size_t x) { return x == 2; }, 1)};
    EXPECT_TRUE(emplace_result);
    EXPECT_EQ(*emplace_result, 1);
    // Before doing other operations, we need to unlock the new entry.
    emplace_result.unlock();
    // -- `l` should be [1, 2, 4, 5, 7, 8, 10, 11].
    l.emplaceBefore([](size_t x) { return x >= 4; }, 3);
    // -- `l` should be [1, 2, 3, 4, 5, 7, 8, 10, 11].
    // > Remove all elements that are greater than 5 from `l`.
    while (l.erase([](size_t x) { return x > 5; }).second) {
    }
    // -- `l` should be [1, 2, 3, 4, 5].
  }

  {  // Test `popFront()`.
    // > Pop the first 4 elements.
    for (size_t i{0}; i < 4; ++i) {
      auto pop_result{l.popFront()};
      EXPECT_TRUE(pop_result.second);
      EXPECT_EQ(*pop_result.first, i + 2);
    }
    // -- `l` should be [5].
    // > Pop the first element.
    {
      auto pop_result{l.popFront()};
      EXPECT_TRUE(pop_result.second);
      // The returned `UniqueLock` should not have a value now.
      EXPECT_FALSE(pop_result.first);
    }
    // -- `l` should be [].
    EXPECT_TRUE(l.empty());
    // > Try `popFront()` again. It should fail this time.
    {
      auto pop_result{l.popFront()};
      EXPECT_FALSE(pop_result.second);
      EXPECT_FALSE(pop_result.first);
    }
  }
}

TEST(ConcurrentForwardList, LockedEntry) {
  using List = ConcurrentForwardList<size_t, shared_mutex>;

  List l;
  // Test copying `SharedEntry`.
  {
    List::SharedEntry entry_1{as_const(l).begin()};
    List::SharedEntry entry_2{entry_1};
    EXPECT_EQ(entry_1, entry_2);
    EXPECT_FALSE(entry_1.hasValue());
    EXPECT_TRUE(entry_1.ownsLock());
    EXPECT_FALSE(entry_2.hasValue());
    EXPECT_TRUE(entry_2.ownsLock());
    List::SharedEntry entry_3;
    entry_3 = entry_1;
    EXPECT_EQ(entry_1, entry_3);
    EXPECT_FALSE(entry_1.hasValue());
    EXPECT_TRUE(entry_1.ownsLock());
    EXPECT_FALSE(entry_3.hasValue());
    EXPECT_TRUE(entry_3.ownsLock());

    entry_1.unlock();
    List::SharedEntry entry_4{entry_1};
    EXPECT_EQ(entry_1, entry_4);
    EXPECT_FALSE(entry_1.hasValue());
    EXPECT_FALSE(entry_1.ownsLock());
    EXPECT_FALSE(entry_4.hasValue());
    EXPECT_FALSE(entry_4.ownsLock());
    entry_3 = entry_1;
    EXPECT_EQ(entry_1, entry_3);
    EXPECT_FALSE(entry_1.hasValue());
    EXPECT_FALSE(entry_1.ownsLock());
    EXPECT_FALSE(entry_3.hasValue());
    EXPECT_FALSE(entry_3.ownsLock());
  }

  // Test operations related to copying `SharedEntry`.
  {
    l.emplaceBack(0);
    l.emplaceBack(1);
    l.emplaceBack(2);
    l.emplaceBack(3);
    l.emplaceBack(4);
    for (size_t i{0}; i < 4; ++i) {
      List::SharedEntry entry_1{as_const(l).begin() + i};
      List::SharedEntry entry_2{entry_1 + 0};
      EXPECT_TRUE(entry_1.hasValue());
      EXPECT_TRUE(entry_1.ownsLock());
      EXPECT_TRUE(entry_2.hasValue());
      EXPECT_TRUE(entry_2.ownsLock());
      EXPECT_EQ(entry_1, entry_2);
      EXPECT_EQ(*entry_2, i);
      entry_2 = entry_1 + 1;
      EXPECT_NE(entry_1, entry_2);
      EXPECT_TRUE(entry_2.ownsLock());
    }
    for (size_t i{0}; i < 4; ++i) {
      List::SharedEntry entry_1{as_const(l).begin()};
      entry_1 += i;
      EXPECT_EQ(*entry_1, i);
    }
    for (size_t i{0}; i < 4; ++i) {
      List::SharedEntry entry_1{as_const(l).begin()};
      List::SharedEntry entry_2 = entry_1++;
      EXPECT_TRUE(entry_1.ownsLock());
      EXPECT_TRUE(entry_2.ownsLock());
      EXPECT_NE(entry_2, entry_1);
      EXPECT_EQ(entry_2 + 1, entry_1);
    }
  }
}
